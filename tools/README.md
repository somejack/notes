# Web Development Tools And Resources

## Platforms

### Runtimes

[NodeJS](https://nodejs.org/en/) - JavaScript runtime built on Chrome's V8 JavaScript engine

### Languages

[HTML5](https://www.w3.org/TR/html/) - Markup language, the latest version of HTML and XHTML

[CSS3](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS3) - Latest version of cascading style sheets used in front-end development of sites and applications

[Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript) - Programming language of HTML and the web

[SQL](https://en.wikipedia.org/wiki/SQL) - Stands for structured query language used with relational databases

### Compilers

[TypeScript](http://www.typescriptlang.org) - Superset of JavaScript that compiles to clean JavaScript output

[Flow](https://flow.org) - Adds static typing to JavaScript to improve developer productivity and code quality

[Babel](https://babeljs.io) - Babel is a compiler for writing next generation JavaScript

## Libraries, Frameworks

### Desktop

[Electron](https://electronjs.org) - Build cross platform desktop apps with JavaScript, HTML, and CSS

### Mobile

[React Native](http://facebook.github.io/react-native/) - A framework for building native apps with React

### Web

[React](https://reactjs.org) - A declarative, efficient, and flexible JavaScript library for buildiing user interfaces

[Preact](https://preactjs.com) - Fast 3kB React alternative with the same modern API. Components & Virtual DOM

[Vue](https://vuejs.org) - A progressive, incrementally-adoptable JavaScript framework for building UI on the web

[Angular](https://angular.io) - One framework. Mobile & desktop

### Node.js

[Express](https://expressjs.com) - Fast, unopinionated, minimalist web framework for node

[Koa](https://koajs.com) - Expressive middleware for node.js using ES2017 async functions

[Nest](https://nestjs.com/) - A progressive Node.js framework for building efficient, reliable and scalable server-side applications

### GraphQL

[GraphQL](https://graphql.org) - Query language and execution engine tied to any backend service

[GraphiQL](https://github.com/graphql/graphiql) - An in-browser IDE for exploring GraphQL

[Relay](https://facebook.github.io/relay/) - A JavaScript framework for building data-driven React applications

[React Starter Kit](https://reactstarter.com) - Isomorphic web app boilerplate (Node.js, Express, GraphQL, React.js, Babel, PostCSS, Webpack, Browsersync)

[Apollo Client](https://www.apollographql.com/client/) - A fully-featured, production ready caching GraphQL client for every UI framework and GraphQL server

[Apollo Server](https://www.apollographql.com/server) - GraphQL server for Express, Connect, Hapi, Koa and more

[Prisma](https://www.prisma.io) - Turns your database into a realtime GraphQL API.

[GraphQL Yoga](https://github.com/prisma/graphql-yoga) - Fully-featured GraphQL Server with focus on easy setup, performance & great developer experience

[GraphQL Playground](https://github.com/prisma/graphql-playground) - GraphQL IDE for better development workflows (GraphQL Subscriptions, interactive docs & collaboration)

[Graphcool](https://www.graph.cool) - Self-hosted backend-as-a-service to develop serverless GraphQL backends

[Hasura](https://hasura.io/) - Instant Realtime GraphQL on Postgres

[PostGraphile](https://www.graphile.org/postgraphile/) - Instant GraphQL API for PostgreSQL database

[Vulcan](http://vulcanjs.org) - A toolkit to quickly build apps with React, GraphQL & Meteor

[ReactQL](https://reactql.org/) - Universal React + GraphQL starter kit, written in Typescript

[Launchpad](https://launchpad.graphql.com) - Launchpad, the GraphQL server demo platform

[GraphCMS](https://graphcms.com/) - GraphQL CMS

### React

[Create React App](https://github.com/facebook/create-react-app) - Create React apps with no build configuration

[Create React Native App](https://github.com/react-community/create-react-native-app) - Create a React Native app on any OS with no build config

[React boilerplate](https://www.reactboilerplate.com) - A highly scalable, offline-first foundation with the best developer experience and a focus on performance and best practices

[React Navigation](https://reactnavigation.org) - Routing and navigation for your React Native apps

[React Router](https://reacttraining.com/react-router/) - Declarative routing for React

### Static Sites

[Gatsby](https://www.gatsbyjs.org) - Blazing fast static site generator for React.

[Hexo](https://hexo.io) - A fast, simple & powerful blog framework, powered by Node.js

[React Static](https://react-static.js.org) - A progressive static site generator for React

[Next.js](https://nextjs.org) - Framework for server-rendered or statically-exported React apps

### Package Managers, Build Tools, Task Runners

[npm](https://www.npmjs.com) - A package manager for JavaScript

[Yarn](https://yarnpkg.com/en/) - Fast, reliable, and secure dependency management

[Parcel](https://parceljs.org) - Blazing fast, zero configuration web application bundler

[Webpack](https://webpack.js.org) - A bundler for javascript and friends

[Grunt](https://gruntjs.com) - The JavaScript Task Runner

### Testing

[Jest](https://jestjs.io) - Delightful JavaScript Testing

[Mocha](https://mochajs.org) - simple, flexible, fun javascript test framework for node.js & the browser

[Storybook](https://storybook.js.org) - Interactive UI component dev & test: React, React Native, Vue, Angular

### Helpers

[Lodash](https://lodash.com) - A modern JavaScript utility library delivering modularity, performance & extras

[Axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js

[json-server](https://github.com/typicode/json-server) - Get a full fake REST API with zero coding in less than 30 seconds (seriously)

[socket.io](https://socket.io) - Realtime application framework (Node.JS server)

[Moment.js](http://momentjs.com) - Parse, validate, manipulate, and display dates in JavaScript

[Underscore.js](https://underscorejs.org) - Functional programming helpers without extending any built-in objects

## Graphic

### CSS Preprocessors

[Sass](http://sass-lang.com) - The most mature, stable, and powerful professional grade CSS extension language in the world

[Stylus](http://stylus-lang.com) - Expressive, robust, feature-rich CSS language built for nodejs

### CSS In JavaScript

[Styled Components](https://www.styled-components.com) - Visual primitives for the component age. Use the best bits of ES6 and CSS to style your apps without stress

[Polished](https://polished.js.org) - A lightweight toolset for writing styles in JavaScript

[Emotion](https://emotion.sh) - Style as a function of state

[CSS Modules](https://github.com/css-modules/css-modules) - Documentation about css-modules

### UI

[Bootstrap](http://getbootstrap.com) - HTML, CSS, and JS framework for developing responsive, mobile first projects on the web

[Semantic UI](https://semantic-ui.com) - UI component framework based around useful principles from natural language

[Foundation](https://foundation.zurb.com) - Family of responsive front-end frameworks that make it easy to design beautiful responsive websites, apps and emails that look amazing on any device

[Materialize](https://materializecss.com) - A CSS Framework based on Material Design

[Pure](https://purecss.io) - A set of small, responsive CSS modules that you can use in every web project

[Skeleton](http://getskeleton.com) - A Dead Simple, Responsive Boilerplate for Mobile-Friendly Development

[UIkit](https://getuikit.com) - A lightweight and modular front-end framework for developing fast and powerful web interfaces

[Milligram](https://milligram.io) - A minimalist CSS framework

[Susy](http://oddbird.net/susy/) - Responsive layout toolkit for Sass

[Bulma](https://bulma.io) - Modern CSS framework based on Flexbox

[mini.css](https://minicss.org) - A minimal, responsive, style-agnostic CSS framework

[Kube](https://imperavi.com/kube/) - Clean, minimalistic and fast to implement CSS & JS framework for professional developers and designers alike

[Lightning Design System](https://www.lightningdesignsystem.com) - Salesforce Lightning Design System

[Primer](https://primer.style) - Resources, tooling, and design guidelines for building interfaces with GitHub’s design system

[Material UI](https://material-ui.com) - React components that implement Google's Material Design

[Ant Design](https://ant.design) - A UI Design Language

[Ant Design Pro](https://pro.ant.design) - An out-of-box UI solution for enterprise applications

[Ant design Mobile](https://mobile.ant.design) - A configurable Mobile UI

[React-Bootstrap](https://react-bootstrap.github.io) - The most popular front-end framework, rebuilt for React

[React-Toolbox](https://github.com/react-toolbox/react-toolbox/) - A set of React components implementing Google's Material Design specification with the power of CSS Modules

[Blueprint](https://blueprintjs.com) - A React-based UI toolkit for the web

[Semantic UI React](https://react.semantic-ui.com) - The official Semantic-UI-React integration

[Fabric](https://developer.microsoft.com/en-us/fabric#/) - The official front-end framework for building experiences that fit seamlessly into Office and Office 365

[Grommet](https://grommet.io) - A design system made for React.js

[Rebass](https://rebassjs.org) - React primitive UI components built with styled-system

[React-MD](https://react-md.mlaursen.com) - Fully accessible material design styled website using React Components and Sass

[React Desktop](http://reactdesktop.js.org) - React UI Components for macOS High Sierra and Windows 10

[NativeBase](https://nativebase.io) - Essential cross-platform UI components for React Native

### Inspiration

[CodePen](https://codepen.io) - Show off your latest creation and get feedback. Build a test case for that pesky bug. Find example design patterns and inspiration for your projects

[Dribble](https://dribbble.com) - A community of designers sharing screenshots of their work, process, and projects

[Behance](https://www.behance.net) - Another community-driven resource where users can showcase and discover creative work

### Pictures

[Pexels](https://www.pexels.com) - Best free stock photos in one place

### Icons

[Font Awesome](https://fontawesome.com/how-to-use) - The world’s most popular and easiest to use icon set

[Freepik](https://www.freepik.com/free-icons) - Free vector icons for personal and commercial use

[Icomoon](https://icomoon.io/) - Icon Font & SVG Icon Sets

[Feather](https://feathericons.com) - Simply beautiful open source icons

[IconMonster](https://iconmonstr.com) - A free, high quality, monstrously big and continuously growing source of simple icons

[Icons8](https://icons8.com) - An extensive list of highly customizable icons created by a single design team

[IconFinder](https://www.iconfinder.com) - Iconfinder provides beautiful icons to millions of designers and developers

[Fontello](http://fontello.com) - Tool to build custom fonts with icons

[Noun Project](https://thenounproject.com) - Over a million curated icons. Available in both free as well as paid versions for greater customizability

### Charts, 3D, Animation

[D3.js](https://d3js.org) - A JavaScript library for manipulating documents based on data

[Frappé Charts](https://frappe.io/charts) - Simple, responsive, modern SVG Charts with zero dependencies

[echarts](http://echarts.baidu.com) - A powerful, interactive charting and visualization library for browser

[Three.js](https://threejs.org) - JavaScript 3D library

[anime.js](http://animejs.com) - JavaScript Animation Engine

[Animate.css](https://daneden.github.io/animate.css/) - A cross-browser library of CSS animations. As easy to use as an easy thing

### Typography & Fonts

[GridLover](https://www.gridlover.net/try) - Establish a typographic system with modular scale & vertical rhythm.

### Maps

[Leaflet](https://leafletjs.com) - An open-source JavaScript library for mobile-friendly interactive maps

[OpenStreetMap](https://www.openstreetmap.org) - A map of the world

## SW

### IDEs & Editors

[VS Code](https://code.visualstudio.com) - Visual Studio Code

[CodeSandbox](https://codesandbox.io) - The online code editor

### API

[Postman](https://www.getpostman.com) - Complete API development environment. Everything from designing, testing, monitoring, and publishing

### Web Servers

[Nginx](https://www.nginx.com) - An open source and high-performant web server. Handles static content well and is lightweight

### Databases

[PostgreSQL](https://www.postgresql.org) - The world's most advanced open source relational database

### Git

[GitLab](https://about.gitlab.comgithub) - Host your private and public software projects for free

[GitHub](https://github.com) - Web-based hosting service for version control using Git

### Collaboration

[Slack](https://slack.com) - Messaging app for teams that is on a mission to make your working life simpler, more pleasant, and more productive

[Trello](https://trello.com) - Flexible and visual way to organize anything with anyone

### Website Test

[Google PageSpeed Insights](https://developers.google.com/speed/pagespeed/insights/?hl=en-US&utm_source=PSI&utm_medium=incoming-link&utm_campaign=PSI) - PageSpeed Insights analyzes the content of a web page, then generates suggestions to make that page faster

[Google Chrome DevTools](https://developers.google.com/web/tools/chrome-devtools/?hl=en) - Set of web authoring and debugging tools built into Google Chrome
